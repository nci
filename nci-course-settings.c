/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

/* Generic max */
static size_t
max(size_t a, size_t b)
{
        return (a > b) ? a : b;
}

/* Compare grading standard entries based on score */
static int
gse_cmp(const void *a, const void *b)
{
        grading_standard_entry *ga = (grading_standard_entry *) a;
        grading_standard_entry *gb = (grading_standard_entry *) b;
        float fa = strtof(ga->value, 0);
        float fb = strtof(gb->value, 0);

        if (fa > fb) {
                return -1;
        } else if (fa < fb) {
                return 1;
        }

        return 0;
}

/* Cut up str into pairs, filling gse as appropriate */
static int
parse_cutoff_arg(char *str, grading_standard_entry **out, size_t *out_num)
{
        char *p1 = 0;
        char *p2 = 0;
        size_t num_gses = 1;
        size_t idx = 0;
        uint_fast8_t should_quit = 0;
        int ret = 0;

        for (p1 = str; *p1; ++p1) {
                num_gses += (*p1 == ',');
        }

        if (!(*out = calloc(num_gses, sizeof(**out)))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        (*out)[0] = (grading_standard_entry) { 0 };
        should_quit = 0;

        for (p2 = str, p1 = str; !should_quit; ++p1) {
                if (*p1 == ',' ||
                    *p1 == '\0') {
                        should_quit = (*p1 == '\0');
                        *p1 = '\0';
                        p1++;
                        (*out)[idx++].name = p2;
                        p2 = p1;
                }
        }

        for (idx = 0; idx < num_gses; ++idx) {
                p2 = strchr((*out)[idx].name, ':');

                if (p2) {
                        *p2 = '\0';
                        p2++;
                        (*out)[idx].value = p2;
                } else {
                        fprintf(stderr, "invalid cutoff entry \"%s\"\n",
                                (*out)[idx].name);
                        ret = EINVAL;
                        goto cleanup;
                }

                p1 = 0;
                strtof(p2, &p1);

                if (p1 &&
                    *p1) {
                        fprintf(stderr, "invalid grade \"%s\"\n", p2);
                        ret = EINVAL;
                        goto cleanup;
                }
        }

cleanup:
        *out_num = num_gses;

        return ret;
}

static int
get_and_write_everything(const char *url_base, const char *auth_token,
                         char *course_id)
{
        int ret = EINVAL;
        size_t len = 0;
        char *built_uri = 0;
        const char *fn_course[] = { "id", "grading_standard_id",
                                    "hide_final_grades",
                                    "apply_assignment_group_weights",
                                    "start_at", "end_at", 0 };
        const char *fn_gse[] = { "name", "value", 0 };
        char **letters = 0;
        size_t letters_num;
        char **c = 0;
        map course = { 0 };
        map grading_standard = { 0 };
        grading_standard_entry *sorted_gse = 0;
        size_t j = 0;
        float f;
        size_t letter_len = 0;
        char no_value[] = { '0', '.', '0', 0 };

        len = snprintf(0, 0, "%s/api/v1/courses/%s", url_base, course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s", url_base, course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_course, 0,
                                     &course))) {
                goto cleanup;
        }

        c = map_get(&course, course_id);

        if (!c) {
                fprintf(stderr, "no course %s\n", course_id);
                ret = EINVAL;
                goto cleanup;
        }

        if (!c) {
                fprintf(stderr, "no course %s\n", course_id);
                ret = EINVAL;
                goto cleanup;
        }

        printf("Hide final grades: %s\n", UBSAFES(c[1]));
        printf("Apply assignment group weights: %s\n", UBSAFES(c[2]));
        printf("Grading standard enabled: %s\n", (c[0] ? "true" : "false"));
        printf("Start date: %s\n", UBSAFES(c[3]));
        printf("End date: %s\n", UBSAFES(c[4]));
        printf("Letter grades:\n");

        if (!c[0]) {
                printf("  No grading standard\n");
                ret = 0;
                goto cleanup;
        }

        free(built_uri);
        len = snprintf(0, 0, "%s/api/v1/courses/%s/grading_standards", url_base,
                       course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/grading_standards", url_base,
                course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_gse, c[0],
                                     &grading_standard))) {
                goto cleanup;
        }

        map_get_keys(&grading_standard, &letters, &letters_num);

        if (!letters_num) {
                printf("  Empty (or Canvas-default?) grading standard\n");
                ret = 0;
                goto cleanup;
        }

        if (!(sorted_gse = calloc(letters_num, sizeof *sorted_gse))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        for (j = 0; j < letters_num; ++j) {
                sorted_gse[j] = (grading_standard_entry) { 0 };
                sorted_gse[j].name = letters[j];
                c = map_get(&grading_standard, letters[j]);

                if (c &&
                    c[0]) {
                        sorted_gse[j].value = c[0];
                } else {
                        sorted_gse[j].value = no_value;
                }

                letter_len = max(letter_len, strlen(letters[j]));
        }

        qsort(sorted_gse, letters_num, sizeof(sorted_gse[0]), gse_cmp);

        for (j = 0; j < letters_num; ++j) {
                f = 100.0 * strtof(sorted_gse[j].value, 0);
                len = snprintf(0, 0, "%.6g%%", f);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(sorted_gse[j].value = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        j++;

                        while (j < letters_num) {
                                sorted_gse[j++].value = 0;
                        }

                        goto cleanup;
                }

                sprintf(sorted_gse[j].value, "%.6g%%", f);
        }

        printf("  %s%*s [%s, \u221e)\n", sorted_gse[0].name, (int) (letter_len -
                                                                    strlen(
                                                                            sorted_gse
                                                                            [0].
                                                                            name)),
               "", sorted_gse[0].value);

        for (j = 1; j < letters_num; ++j) {
                printf("  %s%*s [%s, %s)\n", sorted_gse[j].name,
                       (int) (letter_len - strlen(sorted_gse[j].name)), "",
                       sorted_gse[j].value, sorted_gse[j - 1].value);
        }

        ret = 0;
cleanup:
        map_clean(&course);
        free(built_uri);

        if (sorted_gse) {
                for (j = 0; j < letters_num; ++j) {
                        if (sorted_gse[j].value != no_value) {
                                free(sorted_gse[j].value);
                        }
                }
        }

        free(sorted_gse);

        return ret;
}

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        const char *hfg_arg = 0;
        const char *aagw_arg = 0;
        uint_fast8_t disable_grading_standard = 0;
        char *start_date = 0;
        char *end_date = 0;
        char *cutoff_arg = 0;
        char *gse_id = 0;
        grading_standard_entry *cutoffs = 0;
        size_t cutoffs_num = 0;
        size_t len = 0;
        char *built_uri = 0;
        struct curl_httppost *post = 0;
        uint_fast8_t get_something = 0;
        uint_fast8_t set_something = 0;
        int opt = 0;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "c:dhHwWLs:e:C:")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                case 'd':
                        get_something = 1;
                        break;
                case 'h':
                case 'H':
                        set_something = 1;
                        hfg_arg = (opt == 'h') ? "true" : "false";
                        break;
                case 'w':
                case 'W':
                        set_something = 1;
                        aagw_arg = (opt == 'w') ? "true" : "false";
                        break;
                case 'L':
                        set_something = 1;
                        disable_grading_standard = 1;
                        break;
                case 's':
                        set_something = 1;
                        start_date = optarg;
                        break;
                case 'e':
                        set_something = 1;
                        end_date = optarg;
                        break;
                case 'C':
                        set_something = 1;
                        cutoff_arg = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        if (!set_something &&
            !get_something) {
                ret = EINVAL;
                fprintf(stderr, "no action specified\n");
                goto cleanup;
        }

        if (set_something &&
            get_something) {
                ret = EINVAL;
                fprintf(stderr, "-g is mutually exclusive with setting data\n");
                goto cleanup;
        }

        if (cutoff_arg &&
            (ret = parse_cutoff_arg(cutoff_arg, &cutoffs, &cutoffs_num))) {
                /* Error should have already been printed */
                goto cleanup;
        }

        if (disable_grading_standard &&
            cutoff_arg) {
                ret = EINVAL;
                fprintf(stderr, "-L and -C are mutually exclusive\n");
                goto cleanup;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        /* Reading is so complicated, it gets a separate function */
        if (get_something) {
                ret = get_and_write_everything(url_base, auth_token, course_id);
                goto cleanup;
        }

        /* Cutoffs */
        if (cutoffs_num) {
                len = snprintf(0, 0, "%s/api/v1/courses/%s/grading_standards",
                               url_base, course_id);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(built_uri = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(built_uri, "%s/api/v1/courses/%s/grading_standards",
                        url_base, course_id);

                if ((ret = make_gse_post(cutoffs, cutoffs_num, &post))) {
                        goto cleanup;
                }

                if ((ret = send_and_id_scan(built_uri, auth_token, post, "POST",
                                            &gse_id))) {
                        goto cleanup;
                }

                curl_formfree(post);
                post = 0;
        }

        /* Everything else */
        len = snprintf(0, 0, "%s/api/v1/courses/%s", url_base, course_id);
        free(built_uri);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s", url_base, course_id);

        if ((ret = make_course_post(hfg_arg, aagw_arg, disable_grading_standard,
                                    start_date, end_date, gse_id, &post))) {
                goto cleanup;
        }

        if ((ret = send_and_id_scan(built_uri, auth_token, post, "PUT", 0))) {
                goto cleanup;
        }

        ret = 0;
cleanup:
        curl_formfree(post);
        free(built_uri);
        free(cutoffs);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
