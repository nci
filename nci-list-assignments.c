/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

enum {
        FN_AG_POSITION = 0,
        FN_AG_NAME = 1,
        FN_AG_ID = 2,
        FN_AG_WEIGHT = 3,
        FN_AG_DROP_LOWEST = 4,
        FN_AG_DROP_HIGHEST = 5,
        FN_AG_END = 6,
};

enum {
        FN_A_ID = 0,
        FN_A_GROUP_ID = 1,
        FN_A_POSITION = 2,
        FN_A_NAME = 3,
        FN_A_MAX_POINTS = 4,
        FN_A_DUE_AT = 5,
        FN_A_END = 6
};

static int
cmp_entry_by_pos(const void *a, const void *b)
{
        const entry *ea = (const entry *) a;
        const entry *eb = (const entry *) b;
        const char *sa = ea->vs[FN_A_POSITION - 1];
        const char *sb = eb->vs[FN_A_POSITION - 1];
        long long la = strtoll(sa, 0, 0);
        long long lb = strtoll(sb, 0, 0);

        return (la < lb) ? -1 : ((la > lb) ? 1 : 0);
}

static int
write_out_everything(map *assigns, map *agroups)
{
        int ret = EINVAL;
        size_t j = 0;
        size_t k = 0;
        char **agroups_positions = 0;
        size_t agroups_num = 0;
        char **ag = 0;
        char **a_ids = 0;
        char **unprinted_a_ids = 0;
        size_t as_num = 0;
        char **a = 0;
        entry *ca = 0;
        size_t ca_num = 0;
        uint_fast8_t some_categorized = 0;
        uint_fast8_t printed_uncategorized = 0;

        map_get_keys(agroups, &agroups_positions, &agroups_num);
        map_get_keys(assigns, &a_ids, &as_num);

        if (!(unprinted_a_ids = calloc(as_num, sizeof *unprinted_a_ids))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        if (!(ca = calloc(as_num, sizeof *ca))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        memcpy(unprinted_a_ids, a_ids, as_num * sizeof unprinted_a_ids);

        for (j = 0; j < agroups_num; ++j) {
                ag = map_get(agroups, agroups_positions[j]);

                if (!ag) {
                        continue;
                }

                printf("%s %s", UBSAFES(ag[FN_AG_ID - 1]), UBSAFES(
                               ag[FN_AG_NAME - 1]));

                if (ag[FN_AG_WEIGHT - 1]) {
                        printf(" (%s%%)", ag[FN_AG_WEIGHT - 1]);
                }

                if (ag[FN_AG_DROP_LOWEST - 1] &&
                    strcmp(ag[FN_AG_DROP_LOWEST - 1], "0")) {
                        printf(" (Drop %s lowest)", ag[FN_AG_DROP_LOWEST - 1]);
                }

                if (ag[FN_AG_DROP_HIGHEST - 1] &&
                    strcmp(ag[FN_AG_DROP_HIGHEST - 1], "0")) {
                        printf(" (Drop %s highest)\n", ag[FN_AG_DROP_HIGHEST -
                                                          1]);
                } else {
                        printf("\n");
                }

                ca_num = 0;

                for (k = 0; k < as_num; ++k) {
                        a = map_get(assigns, a_ids[k]);

                        if (!strcmp(ag[FN_AG_ID - 1], a[FN_A_GROUP_ID - 1])) {
                                unprinted_a_ids[k] = 0;
                                ca[ca_num] = (entry) { .k = a_ids[k], .vs = a };
                                ca_num++;
                        }
                }

                qsort(ca, ca_num, sizeof *ca, cmp_entry_by_pos);

                for (k = 0; k < ca_num; ++k) {
                        some_categorized = 1;
                        printf("  %s %s", UBSAFES(ca[k].k), UBSAFES(
                                       ca[k].vs[FN_A_NAME - 1]));

                        if (ca[k].vs[FN_A_MAX_POINTS - 1]) {
                                printf(" (%s)", ca[k].vs[FN_A_MAX_POINTS - 1]);
                        }

                        if (ca[k].vs[FN_A_DUE_AT - 1]) {
                                printf(" due at %s\n", ca[k].vs[FN_A_DUE_AT -
                                                                1]);
                        } else {
                                printf("\n");
                        }
                }
        }

        for (k = 0; k < as_num; ++k) {
                if (!unprinted_a_ids[k]) {
                        continue;
                }

                if (some_categorized &&
                    !printed_uncategorized) {
                        printf("UNCATEGORIZED\n");
                        printed_uncategorized = 1;
                }

                if (some_categorized) {
                        printf("  ");
                }

                a = map_get(assigns, unprinted_a_ids[k]);
                printf("%s %s", unprinted_a_ids[k], a[FN_A_NAME - 1]);

                if (a[FN_A_MAX_POINTS - 1]) {
                        printf(" (%s)\n", a[FN_A_MAX_POINTS - 1]);
                } else {
                        printf("\n");
                }
        }

        ret = 0;
cleanup:
        free(agroups_positions);
        free(a_ids);
        free(unprinted_a_ids);
        free(ca);

        return ret;
}

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        const char *fn_agroups[] = {
                /* */
                [FN_AG_POSITION] = "position",         /* */
                [FN_AG_NAME] = "name",                 /* */
                [FN_AG_ID] = "id",                     /* */
                [FN_AG_WEIGHT] = "group_weight",       /* */
                [FN_AG_DROP_LOWEST] = "drop_lowest",   /* */
                [FN_AG_DROP_HIGHEST] = "drop_highest", /* */
                [FN_AG_END] = 0                        /* */
        };
        const char *fn_assigns[] = {
                /* */
                [FN_A_ID] = "id",                        /* */
                [FN_A_GROUP_ID] = "assignment_group_id", /* */
                [FN_A_POSITION] = "position",            /* */
                [FN_A_NAME] = "name",                    /* */
                [FN_A_MAX_POINTS] = "points_possible",   /* */
                [FN_A_DUE_AT] = "due_at",                /* */
                [FN_A_END] = 0                           /* */
        };
        map agroups = { 0 };
        map assigns = { 0 };
        size_t len = 0;
        char *built_uri = 0;
        int opt;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "c:")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        len = snprintf(0, 0,
                       "%s/api/v1/courses/%s/assignment_groups?per_page=9999",
                       url_base,
                       course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri,
                "%s/api/v1/courses/%s/assignment_groups?per_page=9999",
                url_base,
                course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_agroups, 0,
                                     &agroups))) {
                goto cleanup;
        }

        free(built_uri);
        built_uri = 0;
        len = snprintf(0, 0, "%s/api/v1/courses/%s/assignments?per_page=9999",
                       url_base, course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/assignments?per_page=9999",
                url_base, course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_assigns, 0,
                                     &assigns))) {
                goto cleanup;
        }

        if ((ret = write_out_everything(&assigns, &agroups))) {
                goto cleanup;
        }

        ret = 0;
cleanup:
        free(built_uri);
        map_clean(&agroups);
        map_clean(&assigns);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
