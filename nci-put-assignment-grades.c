/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

int
prepare_csv_for_upload(char ***csv, size_t rows, size_t cols, long
                       long **course_ids, long long **assignment_ids)
{
        size_t j = 0;
        long long course_id = 0;
        long long assignment_id = 0;
        char *past = 0;
        char *inv = 0;
        int sverr = 0;
        uint_fast8_t some_invalid = 0;

        if (!(*course_ids = calloc((cols - 2), sizeof **course_ids))) {
                sverr = errno;
                perror(L("calloc"));

                return sverr;
        }

        if (!(*assignment_ids = calloc((cols - 2), sizeof **assignment_ids))) {
                sverr = errno;
                perror(L("calloc"));

                return sverr;
        }

        if (rows < 2) {
                fprintf(stderr, "CSV has too few rows to contain data");

                return EINVAL;
        }

        if (cols < 3) {
                fprintf(stderr, "CSV has too few cols to contain data");

                return EINVAL;
        }

        if (!csv[0][0] ||
            strcmp(csv[0][0], "Name") ||
            !csv[0][1] ||
            strcmp(csv[0][1], "ID")) {
                fprintf(stderr, "CSV does not contain required header\n");

                return EINVAL;
        }

        for (j = 1; j < rows; ++j) {
                if (csv[j] &&
                    csv[j][1] &&
                    csv[j][1][0]) {
                        strtoll(csv[j][1], &inv, 0);

                        if (inv &&
                            *inv) {
                                fprintf(stderr,
                                        "Student ID in row %zu invalid\n", j);
                                some_invalid = 1;
                        }
                }
        }

        if (some_invalid) {
                return EINVAL;
        }

        for (j = 2; j < cols; ++j) {
                if (!csv[0][j]) {
                        fprintf(stderr,
                                "CSV header cell %zu is blank - must contain "
                                "<course-id>:assignment-id>\n", j + 1);

                        return EINVAL;
                }

                past = 0;
                course_id = strtoll(csv[0][j], &past, 0);

                if (!past ||
                    !*past) {
                        fprintf(stderr, "CSV header cell %zu does not contain "
                                        "assignment-id - must contain "
                                        "<course-id>:assignment-id>\n", j + 1);

                        return EINVAL;
                }

                if (course_id <= 0) {
                        fprintf(stderr, "CSV header cell %zu references "
                                        "impossible course-id %lld\n", j + 1,
                                course_id);

                        return EINVAL;
                }

                assignment_id = strtoll(past + 1, 0, 0);

                if (assignment_id <= 0) {
                        fprintf(stderr, "CSV header cell %zu references "
                                        "impossible assignment-id %lld\n", j +
                                1, course_id);

                        return EINVAL;
                }

                (*course_ids)[j - 2] = course_id;
                (*assignment_ids)[j - 2] = assignment_id;
        }

        return 0;
}

int
main(void)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        size_t j = 0;
        size_t k = 0;
        size_t len = 0;
        char *built_uri = 0;
        char ***csv = 0;
        long long *course_ids = 0;
        long long *assignment_ids = 0;
        size_t rows = 0;
        size_t cols = 0;
        struct curl_httppost *post = 0;

        setlocale(LC_ALL, "");
        UNUSED(built_uri);
        UNUSED(len);
        UNUSED(j);
        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if ((ret = read_csv(stdin, &csv, &rows, &cols))) {
                fprintf(stderr, "Error with CSV\n");
                goto cleanup;
        }

        if ((ret = prepare_csv_for_upload(csv, rows, cols, &course_ids,
                                          &assignment_ids))) {
                /* Error should have already been printed */
                goto cleanup;
        }

        for (j = 2; j < cols; ++j) {
                free(built_uri);
                len = snprintf(0, 0, "%s/api/v1/courses/%lld/assignments/"
                                     "%lld/submissions/update_grades", url_base,
                               course_ids[j - 2],
                               assignment_ids[j - 2]);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(built_uri = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(built_uri, "%s/api/v1/courses/%lld/assignments/"
                                   "%lld/submissions/update_grades", url_base,
                        course_ids[j - 2],
                        assignment_ids[j - 2]);

                if (!(post = make_update_grade_post(csv, j, rows))) {
                        /* XXX: pass back proper error code from make_update_grade_post() */
                        ret = ENOMEM;
                        goto cleanup;
                }

                if ((ret = send_and_id_scan(built_uri, auth_token, post, "POST",
                                            0))) {
                        goto cleanup;
                }

                curl_formfree(post);
                post = 0;
        }

        ret = 0;
cleanup:

        if (csv) {
                for (j = 0; j < rows; ++j) {
                        if (csv[j]) {
                                for (k = 0; k < cols; ++k) {
                                        free(csv[j][k]);
                                }

                                free(csv[j]);
                        }
                }

                free(csv);
        }

        free(built_uri);
        curl_global_cleanup();
        free(url_base);
        free(auth_token);
        free(course_ids);
        free(assignment_ids);

        return ret;
}
