/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

enum {
        FN_FOL_ID = 0,
        FN_FOL_FULL_NAME = 1,
        FN_FOL_END = 2,
};

enum {
        FN_FIL_ID = 0,
        FN_FIL_FOL_ID = 1,
        FN_FIL_FILENAME = 2,
        FN_FIL_END = 3,
};

static int
write_out_everything(map *folders, map *files)
{
        int ret = EINVAL;
        size_t j = 0;
        size_t k = 0;
        char **folder_keys = 0;
        size_t folder_num = 0;
        char **file_keys = 0;
        size_t file_num = 0;
        char **folder = 0;
        char **file = 0;
        const char *folder_print_name = 0;
        char *printed_files = 0;

        map_get_keys(folders, &folder_keys, &folder_num);
        map_get_keys(files, &file_keys, &file_num);

        if (!(printed_files = calloc(file_num, sizeof *printed_files))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        for (j = 0; j < folder_num; ++j) {
                folder = map_get(folders, folder_keys[j]);

                if (!folder) {
                        continue;
                }

                folder_print_name = folder[FN_FOL_FULL_NAME - 1];

                if (!strncmp(folder_print_name, "course files", 12)) {
                        folder_print_name = folder_print_name + 12;

                        while (folder_print_name[0] == '/') {
                                folder_print_name++;
                        }
                }

                for (k = 0; k < file_num; ++k) {
                        if (printed_files[k]) {
                                continue;
                        }

                        file = map_get(files, file_keys[k]);

                        if (!file) {
                                continue;
                        }

                        if (strcmp(file[FN_FIL_FOL_ID - 1], folder_keys[j])) {
                                continue;
                        }

                        if (folder_print_name[0]) {
                                printf("%s/%s\n", folder_print_name,
                                       file[FN_FIL_FILENAME - 1]);
                        } else {
                                printf("%s\n", file[FN_FIL_FILENAME - 1]);
                        }

                        printed_files[k] = 1;
                }
        }

        for (k = 0; k < file_num; ++k) {
                if (printed_files[k]) {
                        continue;
                }

                file = map_get(files, file_keys[j]);

                if (!file) {
                        continue;
                }

                printf("At unknown location: %s\n", file[FN_FIL_FILENAME - 1]);
        }

        ret = 0;
cleanup:
        free(folder_keys);
        free(file_keys);
        free(printed_files);

        return ret;
}

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        const char *fn_folders[] = {
                /* */
                [FN_FOL_ID] = "id",                    /* */
                [FN_FOL_FULL_NAME] = "full_name",      /* */
                [FN_FOL_END] = 0                       /* */
        };
        const char *fn_files[] = {
                /* */
                [FN_FIL_ID] = "id",                    /* */
                [FN_FIL_FOL_ID] = "folder_id",         /* */
                [FN_FIL_FILENAME] = "filename",        /* */
                [FN_FIL_END] = 0                       /* */
        };
        map folders = { 0 };
        map files = { 0 };
        size_t len = 0;
        char *built_uri = 0;
        int opt;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "c:")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        len = snprintf(0, 0, "%s/api/v1/courses/%s/folders", url_base,
                       course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/folders", url_base, course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_folders, 0,
                                     &folders))) {
                goto cleanup;
        }

        free(built_uri);
        built_uri = 0;
        len = snprintf(0, 0, "%s/api/v1/courses/%s/files", url_base, course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/files", url_base, course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_files, 0,
                                     &files))) {
                goto cleanup;
        }

        if ((ret = write_out_everything(&folders, &files))) {
                goto cleanup;
        }

        ret = 0;
cleanup:
        free(built_uri);
        map_clean(&folders);
        map_clean(&files);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
