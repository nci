/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

static int
list_courses(const char *url_base, const char *auth_token)
{
        size_t j = 0;
        int ret = EINVAL;
        size_t len = 0;
        char *built_uri = 0;
        const char *fn_courses[] = { "id", "name", 0 };
        map courses = { 0 };
        char **courses_ids = 0;
        char **c = 0;
        size_t courses_num = 0;

        len = snprintf(0, 0, "%s/api/v1/courses?per_page=9999", url_base);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses?per_page=9999", url_base);

        if ((ret = key_value_extract(built_uri, auth_token, fn_courses, 0,
                                     &courses))) {
                goto cleanup;
        }

        map_get_keys(&courses, &courses_ids, &courses_num);

        for (j = 0; j < courses_num; ++j) {
                c = map_get(&courses, courses_ids[j]);

                if (c[0]) {
                        printf("%s %s\n", courses_ids[j], c[0]);
                }
        }

cleanup:
        free(courses_ids);
        map_clean(&courses);
        free(built_uri);

        return ret;
}

int
main(void)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;

        setlocale(LC_ALL, "");
        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if ((ret = list_courses(url_base, auth_token))) {
                goto cleanup;
        }

        ret = 0;
cleanup:
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
