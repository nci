.SUFFIXES:
.SUFFIXES: .o .c

CFLAGS ?=
LDFLAGS ?=
PKG_CONFIG ?= pkg-config

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man

CFLAGS+= -std=c99 $(shell $(PKG_CONFIG) --cflags libcurl yajl)
LDFLAGS+= $(shell $(PKG_CONFIG) --libs libcurl yajl)

CFLAGS+= -D_POSIX_C_SOURCE=200809L

# Debug
# CFLAGS+= -g -O0 -Wall -Werror -Wextra

HDR = macros.h util.h
BIN =\
	nci-course-settings \
	nci-create-assignment \
	nci-create-assignment-group \
	nci-delete-assignment \
	nci-delete-assignment-group \
	nci-edit-assignment \
	nci-edit-assignment-group \
	nci-get-assignment-grades \
	nci-list-assignments \
	nci-list-courses \
	nci-list-files \
	nci-put-assignment-grades \
	nci-put-file

OBJ = $(BIN:=.o) util.o
MAN = $(BIN:=.1) nci.1

all: $(BIN)

$(BIN): $(@:=.o) util.o

$(OBJ): $(HDR)

util.o: util.c
	$(CC) $(CFLAGS) -o $@ -c $<

nci-%: nci-%.o
	$(CC) -o $@ $< util.o $(LDFLAGS)

clean:
	rm -f $(BIN) $(OBJ)

.PHONY:
	all clean

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f $(BIN) $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f $(MAN) $(DESTDIR)$(MANDIR)/man1/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f $(BIN)
	cd $(DESTDIR)$(MANDIR)/man1 && rm -f $(MAN)
