/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

/* Write output to stdout */
static void
write_out_everything(char *course_id, map *students, map **grades, map *totals,
                     map *partials, size_t assignments_num,
                     char **assignment_ids, map *assignments)
{
        size_t j;
        size_t k;
        char **student_names = 0;
        char *name;
        char **id;
        char **score;
        char **res;
        size_t students_num;

        map_get_keys(students, &student_names, &students_num);
        printf("\"Name\",\"ID\"");

        for (j = 0; j < assignments_num; ++j) {
                printf(",\"%s:%s (", course_id, assignment_ids[j]);
                res = map_get(assignments, assignment_ids[j]);

                if (res &&
                    res[0]) {
                        print_esc_0x22(res[0]);
                }

                printf(")\"");
        }

        if (totals) {
                printf(",\"Total\",\"Letter Grade\"");
        }

        if (partials) {
                printf(",\"Partial\",\"Partial Letter Grade\"");
        }

        printf("\n");

        for (j = 0; j < students_num; ++j) {
                name = student_names[j];
                id = map_get(students, name);

                if (!id ||
                    !id[0]) {
                        continue;
                }

                printf("\"");
                print_esc_0x22(name);
                printf("\",\"%s\"", id[0]);

                for (k = 0; k < assignments_num; ++k) {
                        score = map_get(&((*grades)[k]), id[0]);
                        printf(",\"");

                        if (score &&
                            score[1] &&
                            !strcasecmp(score[1], "true")) {
                                print_esc_0x22("EX");
                        } else if (score &&
                                   score[0]) {
                                print_esc_0x22(score[0]);
                        }

                        printf("\"");
                }

                if (totals) {
                        score = map_get(totals, id[0]);
                        printf(",\"");

                        if (score &&
                            score[0]) {
                                print_esc_0x22(score[0]);
                        }

                        printf(",\"");

                        if (score &&
                            score[1]) {
                                print_esc_0x22(score[1]);
                        }

                        printf("\"");
                }

                if (partials) {
                        score = map_get(partials, id[0]);
                        printf(",\"");

                        if (score &&
                            score[0] &&
                            score[2]) {
                                print_esc_0x22(score[2]);
                        }

                        printf("\",\"");

                        if (score &&
                            score[1] &&
                            score[3]) {
                                print_esc_0x22(score[3]);
                        }

                        printf("\"");
                }

                printf("\n");
        }

        free(student_names);
}

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        const char *fn_students[] = { "sortable_name", "id", 0 };
        const char *fn_scores[] = { "user_id", "score", "excused", 0 };
        const char *fn_totals[] = { "user_id", "final_score", "final_grade",
                                    "current_score", "current_grade", 0 };
        const char *fn_assignments[] = { "id", "name", 0 };
        uint_fast8_t fetch_all_arg = 0;
        uint_fast8_t totals_arg = 0;
        uint_fast8_t partials_arg = 0;
        char **assignment_ids = 0;
        map students = { 0 };
        map *grades = 0;
        map aggregates = { 0 };
        map assignments = { 0 };
        size_t j = 0;
        size_t assignments_num = 0;
        size_t len = 0;
        char *built_uri = 0;
        int opt = 0;

        setlocale(LC_ALL, "");

        if (!(assignment_ids = calloc((argc / 2), sizeof *assignment_ids))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        while ((opt = getopt(argc, argv, "c:a:Atp")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                case 'a':
                        assignment_ids[assignments_num++] = optarg;
                        break;
                case 'A':
                        fetch_all_arg = 1;
                        break;
                case 't':
                        totals_arg = 1;
                        break;
                case 'p':
                        partials_arg = 1;
                        break;
                default:
                        break;
                }
        }

        if (fetch_all_arg &&
            assignments_num) {
                fprintf(stderr, "both -A and -a provided, ignoring -a\n");
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        if (!fetch_all_arg &&
            !assignments_num &&
            !totals_arg &&
            !partials_arg) {
                ret = EINVAL;
                fprintf(stderr, "one of -a, -A, -t, -p is mandatory\n");
                goto cleanup;
        }

        if (!(grades = calloc(assignments_num, sizeof *grades))) {
                ret = errno;
                perror(L("calloc"));
                goto cleanup;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                /* Error should have already been printed */
                ret = ENOENT;
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                /* Error should have already been printed */
                ret = ENOENT;
                goto cleanup;
        }

        /* Get a list of all students, so we can find out their names */
        len = snprintf(0, 0, "%s/api/v1/courses/%s/students?per_page=9999",
                       url_base, course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/students?per_page=9999",
                url_base, course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_students, 0,
                                     &students))) {
                goto cleanup;
        }

        /* Get a list of all assignments, so we can find out their names */
        free(built_uri);
        len = snprintf(0, 0, "%s/api/v1/courses/%s/assignments?per_page=9999",
                       url_base, course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/assignments?per_page=9999",
                url_base, course_id);

        if ((ret = key_value_extract(built_uri, auth_token, fn_assignments, 0,
                                     &assignments))) {
                goto cleanup;
        }

        if (fetch_all_arg) {
                assignments_num = 0;
                free(assignment_ids);
                assignment_ids = 0;
                free(grades);
                grades = 0;
                map_get_keys(&assignments, &assignment_ids, &assignments_num);

                if (!assignments_num &&
                    errno) {
                        ret = ENOMEM;
                        perror(L("map_get_keys"));
                        goto cleanup;
                }

                if (!(grades = calloc(assignments_num, sizeof *grades))) {
                        ret = errno;
                        perror(L("calloc"));
                        goto cleanup;
                }
        }

        for (j = 0; j < assignments_num; ++j) {
                free(built_uri);
                len = snprintf(0, 0, "%s/api/v1/courses/%s/assignments/%s/"
                                     "submissions?per_page=9999", url_base,
                               course_id,
                               assignment_ids[j]);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(built_uri = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(built_uri, "%s/api/v1/courses/%s/assignments/%s/"
                                   "submissions?per_page=9999", url_base,
                        course_id,
                        assignment_ids[j]);

                if ((ret = key_value_extract(built_uri, auth_token, fn_scores,
                                             0, &(grades[j])))) {
                        goto cleanup;
                }
        }

        if (totals_arg ||
            partials_arg) {
                free(built_uri);
                len = snprintf(0, 0,
                               "%s/api/v1/courses/%s/enrollments?per_page=9999",
                               url_base,
                               course_id);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(built_uri = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(built_uri,
                        "%s/api/v1/courses/%s/enrollments?per_page=9999",
                        url_base,
                        course_id);

                if ((ret = key_value_extract(built_uri, auth_token, fn_totals,
                                             0, &aggregates))) {
                        goto cleanup;
                }
        }

        write_out_everything(course_id, &students, &grades, (totals_arg ?
                                                             &aggregates : 0),
                             (partials_arg ? &aggregates : 0), assignments_num,
                             assignment_ids, &assignments);
        ret = 0;
cleanup:
        free(built_uri);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();
        free(assignment_ids);
        map_clean(&students);
        map_clean(&assignments);
        map_clean(&aggregates);

        if (assignments_num) {
                do {
                        map_clean(&grades[--assignments_num]);
                }       while (assignments_num);
        }

        free(grades);

        return ret;
}
