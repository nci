/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        char *group_id = 0;
        char *name = 0;
        char *weight_arg = 0;
        double weight_d = 0;
        char *weight = 0;
        char *drop_lowest_arg = 0;
        long long drop_lowest_ll = 0;
        char *drop_lowest = 0;
        char *drop_highest_arg = 0;
        long long drop_highest_ll = 0;
        char *drop_highest = 0;
        size_t len = 0;
        char *built_uri = 0;
        struct curl_httppost *post = 0;
        int opt = 0;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "g:c:n:w:d:D:")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                case 'g':
                        group_id = optarg;
                        break;
                case 'n':
                        name = optarg;
                        break;
                case 'w':
                        weight_arg = optarg;
                        break;
                case 'd':
                        drop_lowest_arg = optarg;
                        break;
                case 'D':
                        drop_highest_arg = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        if (!group_id) {
                ret = EINVAL;
                fprintf(stderr, "group-id is mandatory\n");
                goto cleanup;
        }

        if (weight_arg) {
                weight_d = strtod(weight_arg, 0);
                len = snprintf(0, 0, "%lf", weight_d);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(weight = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(weight, "%lf", weight_d);
        }

        if (drop_lowest_arg) {
                drop_lowest_ll = strtoll(drop_lowest_arg, 0, 0);
                len = snprintf(0, 0, "%lld", drop_lowest_ll);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(drop_lowest = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(drop_lowest, "%lld", drop_lowest_ll);
        }

        if (drop_highest_arg) {
                drop_highest_ll = strtoll(drop_highest_arg, 0, 0);
                len = snprintf(0, 0, "%lld", drop_highest_ll);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(drop_highest = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(drop_highest, "%lld", drop_highest_ll);
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        len = snprintf(0, 0, "%s/api/v1/courses/%s/assignment_groups/%s",
                       url_base, course_id, group_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/assignment_groups/%s",
                url_base, course_id, group_id);

        if (!(post = make_agroup_post(name, weight, drop_lowest,
                                      drop_highest))) {
                /* XXX: pass back proper error code from make_agroup_post() */
                goto cleanup;
        }

        if ((ret = send_and_id_scan(built_uri, auth_token, post, "PUT", 0))) {
                goto cleanup;
        }

        ret = 0;
cleanup:
        curl_formfree(post);
        free(built_uri);
        free(drop_highest);
        free(drop_lowest);
        free(weight);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
