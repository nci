#define UNUSED(x) (void) (sizeof(x))

#define STR2(x) #x
#define STR(x) STR2(x)

#define L(x) __FILE__ ":" STR(__LINE__) ": " x

#define UBSAFES(s) ((s) ? (s) : "[None]")

#define NCI_USERAGENT "NCI/1.5.0"
