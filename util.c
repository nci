/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

/* Upper bound on length of an acceptable config file line */
#define LINE_BUF_LEN (1 << 11)

/* How many bytes to read at once during CSV parsing */
#define READ_BUF_LEN (1 << 10)

typedef enum {
        CPS_START_OF_CELL,
        CPS_IN_QUOTED_CELL,
        CPS_IN_NONQUOTED_CELL,
        CPS_JUST_SAW_0x22,
        CPS_JUST_ENDED_CELL,
        CPS_JUST_ENDED_LINE,
        CPS_INVALID,
        CPS_END_OF_INPUT
} csv_parse_state;

static size_t curl_to_devnull(char *ptr, size_t size, size_t nmemb, void *ctx);
static char * get_config_file_contents(const char *config_file_name);
static size_t extract_next_link(char *buffer, size_t size, size_t nitems,
                                void *userdata);
static int ie_yc_number(void *ctx, const char *val, size_t len);
static int ie_yc_map_key(void *ctx, const unsigned char *key, size_t len);
static int ie_yc_string(void *ctx, const unsigned char *val, size_t len);
static int fu_pt1_yc_boolean(void *ctx, int val);
static int fu_pt1_yc_end_map(void *ctx);
static int fu_pt1_yc_map_key(void *ctx, const unsigned char *key, size_t len);
static int fu_pt1_yc_start_map(void *ctx);
static int fu_pt1_yc_string(void *ctx, const unsigned char *val, size_t len);
static int fu_pt1_yc_number(void *ctx, const char *nval, size_t len);
static int kve_yc_boolean(void *ctx, int val);
static int kve_yc_end_map(void *ctx);
static int kve_yc_map_key(void *ctx, const unsigned char *key, size_t len);
static int kve_yc_start_map(void *ctx);
static int kve_yc_string(void *ctx, const unsigned char *val, size_t len);
static int kve_yc_number(void *ctx, const char *nval, size_t len);
static size_t map_hash(const char *s);
yajl_callbacks ie_callbacks = {
        /* */
        .yajl_number = ie_yc_number,   /* */
        .yajl_map_key = ie_yc_map_key, /* */
        .yajl_string = ie_yc_string,   /* */
};
yajl_callbacks fu_pt1_callbacks = {
        /* */
        .yajl_end_map = fu_pt1_yc_end_map,     /* */
        .yajl_map_key = fu_pt1_yc_map_key,     /* */
        .yajl_start_map = fu_pt1_yc_start_map, /* */
        .yajl_string = fu_pt1_yc_string,       /* */
        .yajl_number = fu_pt1_yc_number,       /* */
        .yajl_boolean = fu_pt1_yc_boolean,     /* */
};
yajl_callbacks kve_callbacks = {
        /* */
        .yajl_end_map = kve_yc_end_map,     /* */
        .yajl_map_key = kve_yc_map_key,     /* */
        .yajl_start_map = kve_yc_start_map, /* */
        .yajl_string = kve_yc_string,       /* */
        .yajl_number = kve_yc_number,       /* */
        .yajl_boolean = kve_yc_boolean,     /* */
};
static CURL *
create_curl_handle(void)
{
        CURL *c = curl_easy_init();

        if (!c) {
                return 0;
        }

        curl_easy_setopt(c, CURLOPT_USERAGENT, NCI_USERAGENT);
        curl_easy_setopt(c, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(c, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(c, CURLOPT_MAXREDIRS, 10L);
        curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, curl_to_devnull);
        curl_easy_setopt(c, CURLOPT_WRITEDATA, (void *) 0);

        return c;
}

static size_t
curl_to_devnull(char *ptr, size_t size, size_t nmemb, void *ctx)
{
        UNUSED(ptr);
        UNUSED(size);
        UNUSED(nmemb);
        UNUSED(ctx);

        return size * nmemb;
}

static size_t
curl_to_yajl(char *ptr, size_t size, size_t nmemb, void *ctx)
{
        size_t total = size * nmemb;
        curl_to_yajl_ctx *c = (curl_to_yajl_ctx *) ctx;
        yajl_status ys;
        unsigned char *ye = 0;

        /*
         * As part of some security thing I don't give a damn about,
         * Canvas prepends all their responses with `while(1);' or
         * something.
         */
        if (!c->skipped_chars) {
                while (nmemb &&
                       (*ptr != '[' &&
                        *ptr != '{')) {
                        nmemb--;
                        ptr++;
                }

                if (nmemb &&
                    (*ptr == '[' ||
                     *ptr == '{')) {
                        c->skipped_chars = 1;
                } else {
                        return total;
                }
        }

        ys = yajl_parse(c->yh, (const unsigned char *) ptr, size * nmemb);

        if (ys == yajl_status_client_canceled ||
            ys == yajl_status_error) {
                ye = yajl_get_error(c->yh, 1, (const unsigned char *) ptr,
                                    size * nmemb);
                fprintf(stderr, "yajl: %s\n", ye);
                yajl_free_error(c->yh, ye);

                return 0;
        }

        return total;
}

static size_t
extract_next_link(char *buffer, size_t size, size_t nitems, void *userdata)
{
        size_t total = size * nitems;
        char **storage_location = (char **) userdata;
        char *p = 0;
        char *lt = 0;
        char *terminus = buffer + (total - 1);
        char *dup = 0;

        for (p = buffer; p < terminus - 13; ++p) {
                if (*p == '<') {
                        lt = p;
                } else if (!strncmp(p, ">; rel=\"next\"", 13) &&
                           lt) {
                        if (!(dup = strndup(lt + 1, (p - lt) - 1))) {
                                return 0;
                        }

                        free(*storage_location);
                        *storage_location = dup;
                }
        }

        return total;
}

char *
get_auth_token(void)
{
        return get_config_file_contents("token");
}

static char *
get_config_file_contents(const char *config_file_name)
{
        char *xdg_config_home = getenv("XDG_CONFIG_HOME");
        char *home = getenv("HOME");
        char *path = 0;
        FILE *f;
        char *contents = 0;
        char *extra_newline = 0;
        size_t len = 0;
        size_t j;
        char buf[LINE_BUF_LEN];
        char *b = buf;
        size_t bytes_read = 0;
        uint_fast8_t eof = 0;

        if (!xdg_config_home ||
            !xdg_config_home[0]) {
                if (!home) {
                        fprintf(stderr, "You don't have a $HOME. I give up.\n");
                        goto cleanup;
                }

                len = snprintf(0, 0, "%s/.config/nci/%s", home,
                               config_file_name);

                if (len + 1 < len) {
                        errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(path = malloc(len + 1))) {
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(path, "%s/.config/nci/%s", home, config_file_name);
        } else {
                len = snprintf(0, 0, "%s/nci/%s", xdg_config_home,
                               config_file_name);

                if (len + 1 < len) {
                        errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(path = malloc(len + 1))) {
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(path, "%s/nci/%s", xdg_config_home, config_file_name);
        }

        if (!(f = fopen(path, "r"))) {
                perror(L("fopen"));
                fprintf(stderr, "Could not open %s\n", path);
                goto cleanup;
        }

        while (!eof) {
                if (buf + (LINE_BUF_LEN - 1) <= b) {
                        fprintf(stderr,
                                "File %s is too long - refusing to use it\n",
                                path);
                        goto cleanup;
                }

                bytes_read = fread(b, 1, (buf + (LINE_BUF_LEN - 1) - b), f);
                eof = feof(f);
                b += bytes_read;
        }

        if (b != buf) {
                j = (b - buf) - 1;
                buf[j] = '\0';

                if (j > 1 &&
                    buf[j - 1] == '/') {
                        buf[j - 1] = '\0';
                }
        } else {
                fprintf(stderr, "File %s is empty - it shouldn't be\n", path);
                goto cleanup;
        }

        /*
         * Need not check for overflow here, since the maximum
         * length of buffer is capped at LINE_BUF_LEN
         */
        if (!(contents = malloc((b - buf) + 1))) {
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(contents, "%s", buf);

        if ((extra_newline = strchr(contents, '\n'))) {
                *extra_newline = '\0';
        }

cleanup:

        if (f) {
                fclose(f);
        }

        free(path);

        return contents;
}

char *
get_url_base(void)
{
        return get_config_file_contents("site");
}

static int
ie_yc_number(void *ctx, const char *val, size_t len)
{
        return ie_yc_string(ctx, (const unsigned char *) val, len);
}

static int
ie_yc_map_key(void *ctx, const unsigned char *key, size_t len)
{
        ie_y_ctx *c = (ie_y_ctx *) ctx;

        c->saw_id_key = !strncmp((const char *) key, "id", 2) &&
                        len == 2;
        c->saw_message_key = !strncmp((const char *) key, "message", 7);

        return 1;
}

static int
ie_yc_string(void *ctx, const unsigned char *val, size_t len)
{
        ie_y_ctx *c = (ie_y_ctx *) ctx;
        char *dup = 0;

        if (!c->saw_id_key &&
            !c->saw_message_key) {
                return 1;
        }

        if (c->saw_id_key &&
            !c->id_str) {
                return 1;
        }

        if (!(dup = strndup((const char *) val, len))) {
                perror(L("strndup"));

                return 0;
        }

        if (c->saw_id_key) {
                free(*c->id_str);
                *c->id_str = dup;
        } else {
                c->error_message = dup;
        }

        return 1;
}

int
key_value_extract(const char *original_path, const char *auth_token, const
                  char **field_names, char *enclosing_id, map *m)
{
        int ret = 0;
        CURL *c = 0;
        yajl_handle y = { 0 };
        kve_y_ctx ct = { 0 };
        size_t j = 0;
        curl_to_yajl_ctx ctyc = { 0 };
        CURLcode curl_ret;
        char ce[CURL_ERROR_SIZE];
        long http;
        char *free_after_next_perform = 0;
        char *next_uri = 0;
        char *auth_header = 0;
        size_t len = 0;
        struct curl_slist *custom_headers = 0;
        const char *path = original_path;

        ce[0] = '\0';

        if (!(c = create_curl_handle())) {
                ret = errno = ENOMEM;
                perror(L("create_curl_handle"));
                goto cleanup;
        }

        len = snprintf(0, 0, "Authorization: Bearer %s", auth_token);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(auth_header = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(auth_header, "Authorization: Bearer %s", auth_token);

        if (!(custom_headers = curl_slist_append(custom_headers,
                                                 auth_header))) {
                if (!errno) {
                        errno = ENOMEM;
                }

                perror(L("curl_slist_append"));
                goto cleanup;
        }

        curl_easy_setopt(c, CURLOPT_HTTPHEADER, custom_headers);
        curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, curl_to_yajl);
        curl_easy_setopt(c, CURLOPT_WRITEDATA, (void *) &ctyc);
        curl_easy_setopt(c, CURLOPT_ERRORBUFFER, &ce);
        curl_easy_setopt(c, CURLOPT_HEADERFUNCTION, extract_next_link);
        curl_easy_setopt(c, CURLOPT_HEADERDATA, (void *) &next_uri);

        do {
                next_uri = 0;
                ct = (kve_y_ctx) { .m = m, .field_names = field_names,
                                   .enclosing_id = enclosing_id };

                if (!(y = yajl_alloc(&kve_callbacks, 0, (void *) &ct))) {
                        ret = errno = ENOMEM;
                        perror(L("yajl_alloc"));
                        goto cleanup;
                }

                ctyc = (curl_to_yajl_ctx) { .yh = y };
                curl_easy_setopt(c, CURLOPT_URL, path);
                curl_ret = curl_easy_perform(c);
                curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &http);
                yajl_complete_parse(y);

                if (y) {
                        yajl_free(y);
                        y = 0;
                }

                free(free_after_next_perform);
                free(ct.field0);
                ct.field0 = 0;

                if (ct.otherfields) {
                        for (j = 1; field_names[j]; ++j) {
                                free(ct.otherfields[j - 1]);
                        }
                }

                free(ct.otherfields);
                ct.otherfields = 0;

                if (curl_ret != CURLE_OK ||
                    http / 100 != 2) {
                        if (ct.error_message) {
                                fprintf(stderr, L("Error: HTTP %ld: %s\n"),
                                        http, ct.error_message);
                                ret = EACCES;
                        } else {
                                fprintf(stderr, "%s: %s (http %ld)\n\n%s\n", L(
                                                "libcurl"), curl_easy_strerror(
                                                curl_ret), http,
                                        ce);
                                ret = EINVAL;
                        }

                        break;
                }

                path = next_uri;
                free_after_next_perform = next_uri;
        } while (next_uri);

cleanup:
        free(ct.error_message);

        if (c) {
                curl_easy_cleanup(c);
        }

        curl_slist_free_all(custom_headers);
        free(auth_header);

        if (y) {
                yajl_free(y);
        }

        free(next_uri);

        return ret;
}

static int
kve_yc_end_map(void *ctx)
{
        kve_y_ctx *c = (kve_y_ctx *) ctx;
        int mret = 0;

        if (c->exhausted_enclosing_id) {
                return 1;
        }

        if (c->depth == c->enclosing_id_depth) {
                c->exhausted_enclosing_id = 1;
        }

        if (c->depth-- != c->enclosing_id_depth + 1) {
                return 1;
        }

        if (!c->field0) {
                return 1;
        }

        mret = map_add(c->m, c->field0, c->otherfields);
        c->field0 = 0;
        c->otherfields = 0;

        return mret >= 0;
}

static int
kve_handle(kve_y_ctx *c, const char *s, size_t l)
{
        int truncated_l = (l > 70) ? 70 : l;
        char *dup;
        size_t j = 0;

        if (c->saw_id_key &&
            c->enclosing_id &&
            !c->enclosing_id_depth &&
            l == strlen(c->enclosing_id) &&
            !strncmp(s, c->enclosing_id, l)) {
                c->enclosing_id_depth = c->depth;

                return 0;
        }

        if (c->enclosing_id &&
            (c->exhausted_enclosing_id ||
             !c->enclosing_id_depth)) {
                return 0;
        }

        if (c->idx_of_next_entry < 0 &&
            !c->saw_message_key) {
                return 0;
        }

        /* Need not check for overflow; truncated_l <= 70 */
        dup = malloc(truncated_l + 1);

        if (!dup) {
                perror(L("malloc"));

                return -1;
        }

        snprintf(dup, truncated_l + 1, "%s", s);

        if (c->saw_message_key) {
                c->error_message = dup;

                return 0;
        }

        switch (c->idx_of_next_entry) {
        case 0:
                c->field0 = dup;
                break;
        default:

                if (!c->otherfields) {
                        if (!c->m->num_values_per_entry) {
                                for (j = 0; c->field_names[j]; ++j) {
                                }

                                c->m->num_values_per_entry = j - 1;
                        }

                        if (!(c->otherfields = calloc(
                                      c->m->num_values_per_entry,
                                      sizeof *c->field_names))) {
                                return -1;
                        }
                }

                c->otherfields[c->idx_of_next_entry - 1] = dup;
                break;
        }

        c->idx_of_next_entry = -1;

        return 0;
}

static int
kve_yc_boolean(void *ctx, int val)
{
        if (val) {
                if (kve_handle((kve_y_ctx *) ctx, "true", 4)) {
                        return 0;
                }
        } else {
                if (kve_handle((kve_y_ctx *) ctx, "false", 5)) {
                        return 0;
                }
        }

        return 1;
}

static int
kve_yc_map_key(void *ctx, const unsigned char *key, size_t len)
{
        kve_y_ctx *c = (kve_y_ctx *) ctx;
        const char *s = (const char *) key;
        size_t j;

        c->saw_message_key = (!(strncmp(s, "message", 7)) &&
                              c->depth == 2);
        c->saw_id_key = c->enclosing_id &&
                        len == 2 &&
                        (!(strncmp(s, "id", 2))) &&
                        c->depth == 1;

        if (!strncmp(s, c->field_names[0], len)) {
                c->idx_of_next_entry = 0;

                return 1;
        } else {
                for (j = 1; c->field_names[j]; ++j) {
                        if (!strncmp((const char *) key, c->field_names[j],
                                     len)) {
                                c->idx_of_next_entry = j;

                                return 1;
                        }
                }
        }

        c->idx_of_next_entry = -1;

        return 1;
}

static int
kve_yc_number(void *ctx, const char *nval, size_t len)
{
        if (kve_handle((kve_y_ctx *) ctx, nval, len)) {
                return 0;
        }

        return 1;
}

static int
kve_yc_start_map(void *ctx)
{
        kve_y_ctx *c = (kve_y_ctx *) ctx;

        c->depth++;

        return 1;
}

static int
kve_yc_string(void *ctx, const unsigned char *val, size_t len)
{
        kve_y_ctx *c = (kve_y_ctx *) ctx;
        const char *s = (const char *) val;

        if (kve_handle(c, s, len)) {
                return 0;
        }

        return 1;
}

int
make_gse_post(grading_standard_entry *cutoffs, size_t cutoffs_num, struct
              curl_httppost **out_post)
{
        struct curl_httppost *post = 0;
        struct curl_httppost *postend = 0;
        int ret = EINVAL;
        size_t j = 0;

        if (curl_formadd(&post, &postend, CURLFORM_PTRNAME, "title",
                         CURLFORM_PTRCONTENTS, "Grading Scheme",
                         CURLFORM_END)) {
                ret = errno ? errno : ENOMEM;
                errno = ret;
                perror(L("curl_formadd"));
                goto cleanup;
        }

        for (j = 0; j < cutoffs_num; ++j) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "grading_scheme_entry[][name]",
                                 CURLFORM_PTRCONTENTS,
                                 cutoffs[j].name, CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }

                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "grading_scheme_entry[][value]",
                                 CURLFORM_PTRCONTENTS,
                                 cutoffs[j].value, CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        ret = 0;
cleanup:

        if (ret) {
                curl_formfree(post);
                post = 0;
        }

        *out_post = post;

        return ret;
}

int
make_course_post(const char *hfg, const char *aagw, int
                 disable_grading_standard, const char *start_date, const
                 char *end_date, const
                 char *gse_id, struct curl_httppost **out_post)
{
        struct curl_httppost *post = 0;
        struct curl_httppost *postend = 0;
        int ret = EINVAL;

        if (hfg) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "course[hide_final_grades]",
                                 CURLFORM_PTRCONTENTS, hfg,
                                 CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        if (aagw) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "course[apply_assignment_group_weights]",
                                 CURLFORM_PTRCONTENTS,
                                 aagw, CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        if (disable_grading_standard) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "course[grading_standard_id]",
                                 CURLFORM_PTRCONTENTS, "",
                                 CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        if (start_date) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "course[start_at]", CURLFORM_PTRCONTENTS,
                                 start_date,
                                 CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        if (end_date) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "course[end_at]", CURLFORM_PTRCONTENTS,
                                 end_date,
                                 CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        if (gse_id) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "course[grading_standard_id]",
                                 CURLFORM_PTRCONTENTS, gse_id,
                                 CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        ret = 0;
cleanup:

        if (ret) {
                curl_formfree(post);
                post = 0;
        }

        *out_post = post;

        return ret;
}

struct curl_httppost *
make_agroup_post(const char *name, const char *weight, const char *drop_lowest,
                 const char *drop_highest)
{
        struct curl_httppost *post = 0;
        struct curl_httppost *postend = 0;

        if (name) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME, "name",
                                 CURLFORM_PTRCONTENTS, name, CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        if (weight) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "group_weight", CURLFORM_PTRCONTENTS, weight,
                                 CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        if (drop_lowest) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "rules[drop_lowest]", CURLFORM_PTRCONTENTS,
                                 drop_lowest,
                                 CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        if (drop_highest) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "rules[drop_highest]", CURLFORM_PTRCONTENTS,
                                 drop_highest,
                                 CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        return post;
error:
        curl_formfree(post);

        return 0;
}

struct curl_httppost *
make_assignment_post(const char *name, const char *max_points, const
                     char *due_date, const char *group_id)
{
        struct curl_httppost *post = 0;
        struct curl_httppost *postend = 0;

        if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                         "assignment[submission_types][]", CURLFORM_PTRCONTENTS,
                         "on_paper",
                         CURLFORM_END)) {
                errno = errno ? errno : ENOMEM;
                perror(L("curl_formadd"));
                goto error;
        }

        if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                         "assignment[notify_of_update]", CURLFORM_PTRCONTENTS,
                         "false",
                         CURLFORM_END)) {
                errno = errno ? errno : ENOMEM;
                perror(L("curl_formadd"));
                goto error;
        }

        if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                         "assignment[grading_type]", CURLFORM_PTRCONTENTS,
                         "points",
                         CURLFORM_END)) {
                errno = errno ? errno : ENOMEM;
                perror(L("curl_formadd"));
                goto error;
        }

        if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                         "assignment[published]", CURLFORM_PTRCONTENTS, "true",
                         CURLFORM_END)) {
                errno = errno ? errno : ENOMEM;
                perror(L("curl_formadd"));
                goto error;
        }

        if (name) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "assignment[name]", CURLFORM_PTRCONTENTS, name,
                                 CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        if (max_points) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "assignment[points_possible]",
                                 CURLFORM_PTRCONTENTS, max_points,
                                 CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        if (due_date) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "assignment[due_at]", CURLFORM_PTRCONTENTS,
                                 due_date,
                                 CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        if (group_id) {
                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "assignment[assignment_group_id]",
                                 CURLFORM_PTRCONTENTS,
                                 group_id, CURLFORM_END)) {
                        errno = errno ? errno : ENOMEM;
                        perror(L("curl_formadd"));
                        goto error;
                }
        }

        return post;
error:
        curl_formfree(post);

        return 0;
}

struct curl_httppost *
make_update_grade_post(char ***csv, size_t col, size_t rows)
{
        struct curl_httppost *post = 0;
        struct curl_httppost *postend = 0;
        size_t j = 0;
        char *param_name = 0;
        size_t param_name_len = 0;
        size_t len = 0;

        for (j = 1; j < rows; ++j) {
                if (!csv[j] ||
                    !csv[j][col] ||
                    !csv[j][col][0] ||
                    !csv[j][1] ||
                    !csv[j][1][0]) {
                        continue;
                }

                if (!strcasecmp(csv[j][col], "ex")) {
                        len = snprintf(0, 0, "grade_data[%s][excuse]",
                                       csv[j][1]);

                        if (len > param_name_len) {
                                if (len + 1 < len) {
                                        errno = EOVERFLOW;
                                        perror(L(""));
                                        goto error;
                                }

                                param_name_len = len + 1;

                                if (!(param_name = realloc(param_name,
                                                           param_name_len))) {
                                        perror(L("realloc"));
                                        goto error;
                                }
                        }

                        sprintf(param_name, "grade_data[%s][excuse]",
                                csv[j][1]);

                        if (curl_formadd(&post, &postend, CURLFORM_COPYNAME,
                                         param_name, CURLFORM_PTRCONTENTS,
                                         "true",
                                         CURLFORM_END)) {
                                errno = errno ? errno : ENOMEM;
                                perror(L("curl_formadd"));
                                goto error;
                        }
                } else {
                        len = snprintf(0, 0, "grade_data[%s][posted_grade]",
                                       csv[j][1]);

                        if (len > param_name_len) {
                                if (len + 1 < len) {
                                        errno = EOVERFLOW;
                                        perror(L(""));
                                        goto error;
                                }

                                param_name_len = len + 1;

                                if (!(param_name = realloc(param_name,
                                                           param_name_len))) {
                                        perror(L("realloc"));
                                        goto error;
                                }
                        }

                        sprintf(param_name, "grade_data[%s][posted_grade]",
                                csv[j][1]);

                        if (curl_formadd(&post, &postend, CURLFORM_COPYNAME,
                                         param_name, CURLFORM_PTRCONTENTS,
                                         csv[j][col],
                                         CURLFORM_END)) {
                                errno = errno ? errno : ENOMEM;
                                perror(L("curl_formadd"));
                                goto error;
                        }
                }
        }

        goto done;
error:
        curl_formfree(post);
        post = 0;
done:
        free(param_name);

        return post;
}

/* I hate this function. I hate that I had to write it. */
int
file_upload(const char *uri, const char *auth_token, const char *name, const
            char *path, const char *folder_name)
{
        struct curl_httppost *post1 = 0;
        struct curl_httppost *post2 = 0;
        struct curl_httppost *postend = 0;
        struct stat fstat = { 0 };
        int ret = EINVAL;
        size_t len = 0;
        char *filesize = 0;
        CURL *c1 = 0;
        CURL *c2 = 0;
        curl_to_yajl_ctx ctyc = { 0 };
        yajl_handle y = { 0 };
        fu_pt1_y_ctx ct1 = { 0 };
        CURLcode curl_ret;
        char ce[CURL_ERROR_SIZE];
        long http = 0;
        char *auth_header = 0;
        struct curl_slist *custom_headers = 0;
        size_t j = 0;

        ce[0] = '\0';

        /* Get file information */
        if (stat(path, &fstat)) {
                ret = errno;
                perror(L("stat"));
                fprintf(stderr, "invalid path: %s\n", path);
                goto cleanup;
        }

        len = snprintf(0, 0, "%zu", (size_t) fstat.st_size);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(filesize = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(filesize, "%zu", (size_t) fstat.st_size);

        /* Part 1: post to canvas */
        if (!(c1 = create_curl_handle())) {
                ret = errno = ENOMEM;
                perror(L("create_curl_handle"));
                goto cleanup;
        }

        len = snprintf(0, 0, "Authorization: Bearer %s", auth_token);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(auth_header = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(auth_header, "Authorization: Bearer %s", auth_token);

        if (!(custom_headers = curl_slist_append(custom_headers,
                                                 auth_header))) {
                if (!errno) {
                        errno = ENOMEM;
                }

                perror(L("curl_slist_append"));
                goto cleanup;
        }

        if (!(y = yajl_alloc(&fu_pt1_callbacks, 0, (void *) &ct1))) {
                ret = errno = ENOMEM;
                perror(L("yajl_alloc"));
                goto cleanup;
        }

        curl_easy_setopt(c1, CURLOPT_URL, path);
        curl_easy_setopt(c1, CURLOPT_HTTPHEADER, custom_headers);
        curl_easy_setopt(c1, CURLOPT_WRITEFUNCTION, curl_to_yajl);
        ctyc = (curl_to_yajl_ctx) { .yh = y };
        curl_easy_setopt(c1, CURLOPT_WRITEDATA, (void *) &ctyc);
        curl_easy_setopt(c1, CURLOPT_ERRORBUFFER, &ce);

        if (curl_formadd(&post1, &postend, CURLFORM_PTRNAME, "name",
                         CURLFORM_PTRCONTENTS, name, CURLFORM_END)) {
                ret = errno ? errno : ENOMEM;
                errno = ret;
                perror(L("curl_formadd"));
                goto cleanup;
        }

        if (curl_formadd(&post1, &postend, CURLFORM_PTRNAME, "size",
                         CURLFORM_PTRCONTENTS, filesize, CURLFORM_END)) {
                ret = errno ? errno : ENOMEM;
                errno = ret;
                perror(L("curl_formadd"));
                goto cleanup;
        }

        if (!folder_name ||
            !folder_name[0]) {
                folder_name = "";
        }

        if (curl_formadd(&post1, &postend, CURLFORM_PTRNAME,
                         "parent_folder_path", CURLFORM_PTRCONTENTS,
                         folder_name,
                         CURLFORM_END)) {
                ret = errno ? errno : ENOMEM;
                errno = ret;
                perror(L("curl_formadd"));
                goto cleanup;
        }

        curl_easy_setopt(c1, CURLOPT_URL, uri);
        curl_easy_setopt(c1, CURLOPT_HTTPPOST, post1);
        curl_ret = curl_easy_perform(c1);
        curl_easy_getinfo(c1, CURLINFO_RESPONSE_CODE, &http);
        yajl_complete_parse(y);

        /* Was step 1 successful? */
        if (curl_ret != CURLE_OK ||
            http / 100 != 2) {
                if (ct1.error_message) {
                        fprintf(stderr, L("Error: %s (http %ld) for %s\n"),
                                ct1.error_message, http, uri);
                        ret = EACCES;
                        goto cleanup;
                } else {
                        fprintf(stderr, "%s: %s (http %ld)\n\n%s\n", L(
                                        "libcurl"), curl_easy_strerror(
                                        curl_ret), http, ce);
                        ret = EINVAL;
                        goto cleanup;
                }
        }

        if (!ct1.upload_url) {
                fprintf(stderr, L(
                                "Error: Canvas did not reply with upload url\n"));
                ret = EACCES;
                goto cleanup;
        }

        /* Step 2 */
        if (!(c2 = create_curl_handle())) {
                ret = errno = ENOMEM;
                perror(L("create_curl_handle"));
                goto cleanup;
        }

        curl_easy_setopt(c2, CURLOPT_URL, ct1.upload_url);
        curl_easy_setopt(c2, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(c2, CURLOPT_MAXREDIRS, 3);
        curl_easy_setopt(c2, CURLOPT_ERRORBUFFER, &ce);
        postend = 0;

        for (j = 0; j < ct1.upload_params_len; ++j) {
                if (curl_formadd(&post2, &postend, CURLFORM_PTRNAME,
                                 ct1.upload_params_keys[j],
                                 CURLFORM_PTRCONTENTS,
                                 ct1.upload_params_vals[j], CURLFORM_END)) {
                        ret = errno ? errno : ENOMEM;
                        errno = ret;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        if (curl_formadd(&post2, &postend, CURLFORM_PTRNAME, "file",
                         CURLFORM_FILECONTENT, path, CURLFORM_END)) {
                ret = errno ? errno : ENOMEM;
                errno = ret;
                perror(L("curl_formadd"));
                goto cleanup;
        }

        curl_easy_setopt(c2, CURLOPT_HTTPPOST, post2);
        curl_ret = curl_easy_perform(c2);
        curl_easy_getinfo(c2, CURLINFO_RESPONSE_CODE, &http);

        if (http / 100 != 2) {
                fprintf(stderr, "%s: %s (http %ld)\n\n%s\n", L("libcurl"),
                        curl_easy_strerror(curl_ret), http, ce);
                ret = EINVAL;
                goto cleanup;
        }

        /* Step 3 is in the FOLLOW_REDIRS of step 2. It switches to GET correctly. */
        ret = 0;
cleanup:

        if (c1) {
                curl_easy_cleanup(c1);
        }

        if (c2) {
                curl_easy_cleanup(c2);
        }

        if (y) {
                yajl_free(y);
        }

        for (j = 0; j < ct1.upload_params_len; ++j) {
                free(ct1.upload_params_keys[j]);
                free(ct1.upload_params_vals[j]);
        }

        free(ct1.upload_params_keys);
        free(ct1.upload_params_vals);
        ct1 = (fu_pt1_y_ctx) { 0 };
        curl_formfree(post1);
        post1 = 0;
        curl_formfree(post2);
        post2 = 0;
        curl_slist_free_all(custom_headers);
        free(auth_header);

        return ret;
}

static int
fu_pt1_yc_end_map(void *ctx)
{
        fu_pt1_y_ctx *c = (fu_pt1_y_ctx *) ctx;
        int mret = 0;

        if (c->depth == c->depth_of_upload_params) {
                c->in_upload_params = 0;
        }

        c->depth--;

        return mret >= 0;
}

static int
fu_pt1_handle_val(fu_pt1_y_ctx *c, const char *s, size_t l)
{
        char *dup = 0;

        if (c->next_val_is_url) {
                c->next_val_is_url = 0;

                if (!(dup = malloc(l + 1))) {
                        perror(L("malloc"));

                        return -1;
                }

                snprintf(dup, l + 1, "%s", s);
                free(c->upload_url);
                c->upload_url = dup;

                return 0;
        }

        if (!c->in_upload_params) {
                return 0;
        }

        if (l + 1 < l) {
                errno = EOVERFLOW;
                perror(L(""));

                return -1;
        }

        free(c->upload_params_vals[c->upload_params_len - 1]);

        if (!(dup = malloc(l + 1))) {
                perror(L("malloc"));

                return -1;
        }

        snprintf(dup, l + 1, "%s", s);
        c->upload_params_vals[c->upload_params_len - 1] = dup;

        return 0;
}

static int
fu_pt1_yc_boolean(void *ctx, int val)
{
        if (val) {
                if (fu_pt1_handle_val((fu_pt1_y_ctx *) ctx, "true", 4)) {
                        return 0;
                }
        } else {
                if (fu_pt1_handle_val((fu_pt1_y_ctx *) ctx, "false", 5)) {
                        return 0;
                }
        }

        return 1;
}

static int
fu_pt1_yc_map_key(void *ctx, const unsigned char *key, size_t len)
{
        fu_pt1_y_ctx *c = (fu_pt1_y_ctx *) ctx;
        const char *s = (const char *) key;
        void *new_keys = 0;
        void *new_vals = 0;
        size_t new_len = c->upload_params_len + 1;

        if (!c->in_upload_params) {
                if (len == 13 &&
                    !strncmp(s, "upload_params", len)) {
                        c->in_upload_params = 1;
                        c->depth_of_upload_params = c->depth + 1;

                        return 1;
                }

                if (len == 10 &&
                    !strncmp(s, "upload_url", len)) {
                        c->next_val_is_url = 1;

                        return 1;
                }

                return 1;
        }

        if (new_len < c->upload_params_len) {
                errno = EOVERFLOW;
                perror(L(""));

                return 0;
        }

        if (new_len * (sizeof *(c->upload_params_keys)) / new_len !=
            sizeof *(c->upload_params_keys)) {
                errno = EOVERFLOW;
                perror(L(""));

                return 0;
        }

        if (!(new_keys = realloc(c->upload_params_keys, new_len *
                                 (sizeof *(c->upload_params_keys))))) {
                perror(L("realloc"));

                return 0;
        }

        c->upload_params_keys = new_keys;
        c->upload_params_keys[new_len - 1] = 0;

        if (!(new_vals = realloc(c->upload_params_vals, new_len *
                                 (sizeof *(c->upload_params_vals))))) {
                perror(L("realloc"));

                return 0;
        }

        c->upload_params_vals = new_vals;
        c->upload_params_vals[new_len - 1] = 0;
        c->upload_params_len = new_len;

        if (!(c->upload_params_keys[new_len - 1] = malloc(len + 1))) {
                perror(L("malloc"));

                return 0;
        }

        snprintf(c->upload_params_keys[new_len - 1], len + 1, "%s", s);
        c->upload_params_keys[new_len - 1][len] = '\0';

        return 1;
}

static int
fu_pt1_yc_number(void *ctx, const char *nval, size_t len)
{
        if (fu_pt1_handle_val((fu_pt1_y_ctx *) ctx, nval, len)) {
                return 0;
        }

        return 1;
}

static int
fu_pt1_yc_start_map(void *ctx)
{
        fu_pt1_y_ctx *c = (fu_pt1_y_ctx *) ctx;

        c->depth++;

        return 1;
}

static int
fu_pt1_yc_string(void *ctx, const unsigned char *val, size_t len)
{
        fu_pt1_y_ctx *c = (fu_pt1_y_ctx *) ctx;
        const char *s = (const char *) val;

        if (fu_pt1_handle_val(c, s, len)) {
                return 0;
        }

        return 1;
}

int
map_add(map *m, char *k, char **vs)
{
        size_t j = 0;
        size_t l = 0;
        entry *e = 0;
        void *newmem = 0;
        size_t hash = map_hash(k);

        if (!k ||
            !m) {
                errno = EINVAL;

                return -1;
        }

        for (j = 0; j < m->es[hash]; ++j) {
                e = &m->e[hash][j];

                if (!(strcmp(k, e->k))) {
                        free(k);

                        if (e->vs) {
                                for (l = 0; l < m->num_values_per_entry; ++l) {
                                        free(e->vs[l]);
                                }
                        }

                        free(e->vs);
                        e->vs = vs;

                        return 0;
                }
        }

        if (m->es[hash] + 1 < m->es[hash] ||
            m->es[hash] + 1 >= SIZE_MAX / (sizeof *m->e[hash])) {
                errno = EOVERFLOW;

                return -1;
        }

        if (!(newmem = realloc(m->e[hash], (m->es[hash] + 1) *
                               sizeof *m->e[hash]))) {
                goto malloc_err;
        }

        m->e[hash] = newmem;
        m->e[hash][m->es[hash]] = (entry) { .k = k, .vs = vs };
        m->es[hash]++;

        return 0;
malloc_err:
        errno = ENOMEM;

        return -1;
}

void
map_clean(map *m)
{
        size_t j = 0;
        size_t k = 0;
        size_t l = 0;
        entry *e = 0;

        if (!m) {
                return;
        }

        for (j = 0; j < BUCKET_NUM; ++j) {
                for (k = 0; k < m->es[j]; ++k) {
                        e = &m->e[j][k];
                        free(e->k);

                        if (e->vs) {
                                for (l = 0; l < m->num_values_per_entry; ++l) {
                                        free(e->vs[l]);
                                }
                        }

                        free(e->vs);
                }

                free(m->e[j]);
        }

        *m = (map) { 0 };
}

static int
map_cmp_key(const void *a, const void *b)
{
        const char *s = *((const char **) a);
        const char *t = *((const char **) b);
        long long m = strtoll(s, 0, 0);
        long long n = strtoll(t, 0, 0);

        return (m < n) ? -1 : ((m > n) ? 1 : strcmp(s, t));
}

char **
map_get(map *m, char *k)
{
        size_t j = 0;
        entry *e = 0;
        size_t hash = map_hash(k);

        if (!m ||
            !k) {
                return 0;
        }

        for (j = 0; j < m->es[hash]; ++j) {
                e = &m->e[hash][j];

                if (!(strcmp(k, e->k))) {
                        return e->vs;
                }
        }

        return 0;
}

void
map_get_keys(map *m, char ***out_k, size_t *out_num)
{
        size_t num_keys = 0;
        size_t j = 0;
        size_t k = 0;
        size_t l = 0;

        for (j = 0; j < BUCKET_NUM; ++j) {
                num_keys += m->es[j];
        }

        if (!(*out_k = calloc(num_keys, sizeof **out_k))) {
                *out_num = 0;

                return;
        }

        for (j = 0; j < BUCKET_NUM; ++j) {
                for (k = 0; k < m->es[j]; ++k) {
                        (*out_k)[l++] = m->e[j][k].k;
                }
        }

        qsort(*out_k, num_keys, sizeof **out_k, map_cmp_key);
        *out_num = num_keys;
}

static size_t
map_hash(const char *s)
{
        const char *p = 0;
        uint_fast8_t j;
        size_t hash = 5381;

        if (!s) {
                return 0;
        }

        for (p = s, j = 0; *p &&
             j < 15; ++p, ++j) {
                hash = hash * 33 ^ *p;
        }

        return hash % BUCKET_NUM;
}

void
print_esc_0x22(const char *s)
{
        const char *p = s;

        /* Quote as per RFC 4180 */
        while (*p) {
                if (*p == '"') {
                        putchar('"');
                        putchar('"');
                } else {
                        putchar(*p);
                }

                ++p;
        }
}

static int
process_char_csv(int c, csv_parse_state *s, char **cell, size_t *cell_sz,
                 size_t *cell_pos, char ****csv, char *reading_header,
                 size_t *record_len,
                 size_t *y, size_t *x)
{
        void *newmem;

        if (*cell_pos + 1 >= *cell_sz) {
                if (*cell_sz + 64 < *cell_sz ||
                    (*cell_sz + 64) >= SIZE_MAX / (sizeof **cell)) {
                        errno = EOVERFLOW;
                        perror(L(""));
                        *s = CPS_INVALID;

                        return -1;
                }

                if (!(newmem = realloc(*cell, (*cell_sz + 64) *
                                       sizeof **cell))) {
                        perror(L("realloc"));
                        *s = CPS_INVALID;

                        return -1;
                }

                *cell = newmem;
                *cell_sz += 64;
        }

        switch (*s) {
        case CPS_START_OF_CELL:

                if (c == EOF) {
                        *s = CPS_END_OF_INPUT;
                } else if (c == '"') {
                        *s = CPS_IN_QUOTED_CELL;
                } else if (c == ',') {
                        *s = CPS_JUST_ENDED_CELL;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == '\n') {
                        *s = CPS_JUST_ENDED_LINE;
                        (*cell)[*cell_pos] = '\0';
                } else {
                        (*cell)[(*cell_pos)++] = (char) c;
                        *s = CPS_IN_NONQUOTED_CELL;
                }

                break;
        case CPS_IN_QUOTED_CELL:

                if (c == EOF) {
                        *s = CPS_INVALID;
                } else if (c == '"') {
                        *s = CPS_JUST_SAW_0x22;
                } else {
                        (*cell)[(*cell_pos)++] = (char) c;
                }

                break;
        case CPS_IN_NONQUOTED_CELL:

                if (c == EOF) {
                        *s = CPS_END_OF_INPUT;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == ',') {
                        *s = CPS_JUST_ENDED_CELL;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == '\n') {
                        *s = CPS_JUST_ENDED_LINE;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == '"') {
                        *s = CPS_INVALID;
                } else {
                        (*cell)[(*cell_pos)++] = (char) c;
                }

                break;
        case CPS_JUST_SAW_0x22:

                if (c == EOF) {
                        *s = CPS_END_OF_INPUT;
                } else if (c == '"') {
                        (*cell)[(*cell_pos)++] = '"';
                        *s = CPS_IN_QUOTED_CELL;
                } else if (c == ',') {
                        *s = CPS_JUST_ENDED_CELL;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == '\n') {
                        *s = CPS_JUST_ENDED_LINE;
                        (*cell)[*cell_pos] = '\0';
                } else {
                        *s = CPS_INVALID;
                }

                break;
        case CPS_JUST_ENDED_LINE:
        case CPS_JUST_ENDED_CELL:

                if (c == EOF) {
                        *s = CPS_END_OF_INPUT;
                } else if (c == '\n') {
                        *s = CPS_JUST_ENDED_LINE;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == ',') {
                        *s = CPS_JUST_ENDED_CELL;
                        (*cell)[*cell_pos] = '\0';
                } else if (c == '"') {
                        *s = CPS_IN_QUOTED_CELL;
                } else {
                        (*cell)[(*cell_pos)++] = (char) c;
                        *s = CPS_IN_NONQUOTED_CELL;
                }

                break;
        case CPS_INVALID:
        case CPS_END_OF_INPUT:
                break;
        }

        if (*reading_header &&
            (*s == CPS_JUST_ENDED_CELL ||
             *s == CPS_JUST_ENDED_LINE ||
             *s == CPS_END_OF_INPUT)) {
                (*record_len)++;

                if (*reading_header) {
                        if (*record_len + 1 < *record_len ||
                            (*record_len + 1) >= SIZE_MAX / (sizeof ***csv)) {
                                errno = EOVERFLOW;
                                perror(L(""));

                                return -1;
                        }

                        if (!(newmem = realloc((*csv)[0], (*record_len + 1) *
                                               sizeof ***csv))) {
                                perror(L("realloc"));

                                return -1;
                        }

                        (*csv)[0] = newmem;
                }
        }

        if (*s == CPS_JUST_ENDED_CELL ||
            *s == CPS_JUST_ENDED_LINE ||
            *s == CPS_END_OF_INPUT) {
                if (*x >= *record_len) {
                        *s = CPS_INVALID;

                        return -1;
                }

                (*csv)[*y][*x] = *cell;

                /* No need to check overflow; pointers are tiny */
                if (!(*cell = malloc(1 * sizeof **cell))) {
                        perror(L("malloc"));

                        return -1;
                }

                *cell_sz = 1;
                *cell_pos = 0;
        }

        if (*s == CPS_JUST_ENDED_LINE) {
                (*y)++;
                *x = 0;
                *reading_header = 0;

                if (*y + 1 < *y ||
                    (*y + 1) >= SIZE_MAX / (sizeof *csv)) {
                        errno = EOVERFLOW;
                        perror(L(""));

                        return -1;
                }

                if (!(newmem = realloc(*csv, (*y + 1) * sizeof *csv))) {
                        perror(L("realloc"));

                        return -1;
                }

                *csv = newmem;

                if (!((*csv)[*y] = calloc(*record_len, sizeof ***csv))) {
                        perror(L("calloc"));

                        return -1;
                }
        }

        if (*s == CPS_JUST_ENDED_CELL) {
                (*x)++;
        }

        return 0;
}

int
read_csv(FILE *f, char ****out_csv, size_t *out_rows, size_t *out_cols)
{
        size_t fread_ret = 0;
        csv_parse_state s = CPS_START_OF_CELL;
        char *cell = 0;
        size_t cell_sz = 1;
        size_t cell_pos = 0;
        char ***csv = 0;
        char reading_header = 1;
        size_t record_len = 0;
        size_t y = 0;
        size_t x = 0;
        size_t j;
        size_t k;
        char buf[READ_BUF_LEN];
        const char *buf_end = 0;
        const char *p = 0;

        if (!(cell = calloc(cell_sz, sizeof *cell))) {
                perror(L("calloc"));

                return -1;
        }

        if (!(csv = calloc(1, sizeof *csv))) {
                perror(L("calloc"));

                return -1;
        }

        if (!(csv[0] = calloc(1, sizeof **csv))) {
                perror(L("malloc"));

                return -1;
        }

        while (!feof(f) &&
               !ferror(f)) {
                fread_ret = fread(buf, sizeof *buf, READ_BUF_LEN, f);

                if (!fread_ret) {
                        continue;
                }

                buf_end = buf + (fread_ret - 1);
                p = buf;

                while (p <= buf_end) {
                        if (process_char_csv(*p, &s, &cell, &cell_sz, &cell_pos,
                                             &csv, &reading_header, &record_len,
                                             &y, &x) < 0) {
                                goto error;
                        }

                        p++;
                }
        }

        if (s == CPS_INVALID) {
                goto error;
        }

        y++;
        free(cell);
        *out_csv = csv;
        *out_rows = y;
        *out_cols = record_len;

        return 0;
error:

        for (j = 0; j < y; ++j) {
                for (k = 0; k < x; ++k) {
                        free(csv[j][k]);
                }

                free(csv[j]);
        }

        free(csv);
        free(cell);

        return -1;
}

int
send_and_id_scan(const char *uri, const char *auth_token, struct
                 curl_httppost *post, const char *method, char **id)
{
        int ret = 0;
        CURL *c = 0;
        yajl_handle y = { 0 };
        ie_y_ctx ct = { .id_str = id };
        curl_to_yajl_ctx ctyc = { 0 };
        CURLcode curl_ret;
        char ce[CURL_ERROR_SIZE];
        long http;
        char *auth_header = 0;
        size_t len = 0;
        struct curl_slist *custom_headers = 0;

        ce[0] = '\0';

        if (!(c = create_curl_handle())) {
                ret = errno = ENOMEM;
                perror(L("create_curl_handle"));
                goto cleanup;
        }

        len = snprintf(0, 0, "Authorization: Bearer %s", auth_token);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(auth_header = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(auth_header, "Authorization: Bearer %s", auth_token);

        if (!(custom_headers = curl_slist_append(custom_headers,
                                                 auth_header))) {
                if (!errno) {
                        errno = ENOMEM;
                }

                perror(L("curl_slist_append"));
                ret = errno;
                goto cleanup;
        }

        if (!(y = yajl_alloc(&ie_callbacks, 0, (void *) &ct))) {
                ret = errno = ENOMEM;
                perror(L("yajl_alloc"));
                goto cleanup;
        }

        ctyc = (curl_to_yajl_ctx) { .yh = y };
        curl_easy_setopt(c, CURLOPT_HTTPHEADER, custom_headers);
        curl_easy_setopt(c, CURLOPT_HTTPPOST, post);
        curl_easy_setopt(c, CURLOPT_CUSTOMREQUEST, method);
        curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, curl_to_yajl);
        curl_easy_setopt(c, CURLOPT_WRITEDATA, (void *) &ctyc);
        curl_easy_setopt(c, CURLOPT_ERRORBUFFER, &ce);
        curl_easy_setopt(c, CURLOPT_URL, uri);
        curl_ret = curl_easy_perform(c);
        curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &http);
        yajl_complete_parse(y);

        if (curl_ret != CURLE_OK ||
            http / 100 != 2) {
                if (ct.error_message) {
                        fprintf(stderr, L("Error: %s (http %ld) for %s\n"),
                                ct.error_message, http, uri);
                        ret = EACCES;
                } else {
                        fprintf(stderr, "%s: %s (http %ld)\n\n%s\n", L(
                                        "libcurl"), curl_easy_strerror(
                                        curl_ret), http, ce);
                        ret = EINVAL;
                }
        }

cleanup:
        free(ct.error_message);

        if (c) {
                curl_easy_cleanup(c);
        }

        curl_slist_free_all(custom_headers);
        free(auth_header);

        if (y) {
                yajl_free(y);
        }

        return ret;
}
