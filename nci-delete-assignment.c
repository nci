/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        char *assignment_id = 0;
        size_t len = 0;
        char *built_uri = 0;
        char *deleted_id = 0;
        int opt = 0;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "c:a:")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                case 'a':
                        assignment_id = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        if (!assignment_id) {
                ret = EINVAL;
                fprintf(stderr, "assignment-id is mandatory\n");
                goto cleanup;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        len = snprintf(0, 0, "%s/api/v1/courses/%s/assignments/%s", url_base,
                       course_id, assignment_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/assignments/%s", url_base,
                course_id, assignment_id);

        if ((ret = send_and_id_scan(built_uri, auth_token, 0, "DELETE",
                                    &deleted_id))) {
                goto cleanup;
        }

        if (deleted_id) {
                printf("%s\n", deleted_id);
        }

        ret = 0;
cleanup:
        free(deleted_id);
        free(built_uri);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
