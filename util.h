#define BUCKET_NUM 32

typedef struct {
        /* */
        uint_fast8_t skipped_chars;
        yajl_handle yh;
} curl_to_yajl_ctx;
typedef struct {
        /* */
        char *name;
        char *value;
} grading_standard_entry;

/*
 * Note that the map structure doesn't dup anything, yet does free
 * everything. Once something is added as a key or value to the map,
 * consider it freed. This is true even if the add was for a redundant
 * key.
 */
typedef struct {
        /* */
        char *k;
        char **vs;
} entry;
typedef struct {
        /* */
        size_t es[BUCKET_NUM];
        entry *e[BUCKET_NUM];
        size_t num_values_per_entry;
} map;
typedef struct {
        /* */
        uint_fast8_t saw_id_key;
        char **id_str;
        uint_fast8_t saw_message_key;
        char *error_message;
} ie_y_ctx;
typedef struct {
        /* */
        int idx_of_next_entry;
        const char **field_names;
        char *field0;
        char **otherfields;
        map *m;
        unsigned int depth;
        char *enclosing_id;
        uint_fast8_t saw_id_key;
        unsigned int enclosing_id_depth;
        uint_fast8_t exhausted_enclosing_id;
        uint_fast8_t saw_message_key;
        char *error_message;
} kve_y_ctx;
typedef struct {
        /* */
        unsigned int depth;
        unsigned int depth_of_upload_params;
        uint_fast8_t in_upload_params;
        uint_fast8_t next_val_is_url;
        char *upload_url;
        char **upload_params_keys;
        char **upload_params_vals;
        size_t upload_params_len;
        char *error_message;
} fu_pt1_y_ctx;

char * get_auth_token(void);
char * get_url_base(void);

int key_value_extract(const char *path, const char *cookiejar_path, const
                      char **field_names, char *enclosing_id, map *m);

int make_gse_post(grading_standard_entry *cutoffs, size_t cutoffs_num, struct
                  curl_httppost **out_post);
int make_course_post(const char *hgs, const char *aagw, int
                     disable_grading_standard, const char *start_date, const
                     char *end_date, const
                     char *gse_id, struct curl_httppost **out_post);
struct curl_httppost * make_agroup_post(const char *name, const char *weight,
                                        const char *drop_lowest, const
                                        char *drop_highest);
struct curl_httppost * make_assignment_post(const char *name, const
                                            char *max_points, const
                                            char *due_date, const
                                            char *group_id);
struct curl_httppost * make_update_grade_post(char ***csv, size_t col, size_t
                                              rows);

int file_upload(const char *uri, const char *auth_token, const char *name, const char *path, const
                char *folder_name);

int map_add(map *m, char *k, char **vs);

void map_clean(map *m);

char ** map_get(map *m, char *k);

void map_get_keys(map *m, char ***out_k, size_t *out_num);

void print_esc_0x22(const char *s);

int read_csv(FILE *f, char ****out_csv, size_t *out_rows, size_t *out_cols);

int send_and_id_scan(const char *uri, const char *auth_token, struct
                     curl_httppost *post, const char *method, char **id);
