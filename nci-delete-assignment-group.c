/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        char *group_id = 0;
        char *target_arg = 0;
        long long target_ll = 0;
        char *target = 0;
        uint_fast8_t purge_arg = 0;
        size_t len = 0;
        char *built_uri = 0;
        struct curl_httppost *post = 0;
        struct curl_httppost *postend = 0;
        char *deleted_id = 0;
        int opt = 0;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "c:g:t:p")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                case 'g':
                        group_id = optarg;
                        break;
                case 't':
                        target_arg = optarg;
                        break;
                case 'p':
                        purge_arg = 1;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        if (!target_arg &&
            !purge_arg) {
                ret = EINVAL;
                fprintf(stderr, "one of -t, -p is mandatory\n");
                goto cleanup;
        } else if (target_arg &&
                   purge_arg) {
                fprintf(stderr, "both -t and -p provided, ignoring -p\n");
                purge_arg = 0;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (target_arg) {
                target_ll = strtoll(target_arg, 0, 0);
                len = snprintf(0, 0, "%lld", target_ll);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto cleanup;
                }

                if (!(target = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto cleanup;
                }

                sprintf(target, "%lld", target_ll);

                if (curl_formadd(&post, &postend, CURLFORM_PTRNAME,
                                 "move_assignments_to", CURLFORM_PTRCONTENTS,
                                 target,
                                 CURLFORM_END)) {
                        ret = errno;
                        perror(L("curl_formadd"));
                        goto cleanup;
                }
        }

        len = snprintf(0, 0, "%s/api/v1/courses/%s/assignment_groups/%s",
                       url_base, course_id, group_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/assignment_groups/%s",
                url_base, course_id, group_id);

        if ((ret = send_and_id_scan(built_uri, auth_token, post, "DELETE",
                                    &deleted_id))) {
                goto cleanup;
        }

        if (deleted_id) {
                printf("%s\n", deleted_id);
        }

        ret = 0;
cleanup:
        free(deleted_id);
        curl_formfree(post);
        free(built_uri);
        free(target);
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
