/*
 * Copyright (c) 2016-2019 S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>
#include <yajl_parse.h>

#include "macros.h"
#include "util.h"

int
main(int argc, char **argv)
{
        int ret = EINVAL;
        char *url_base = 0;
        char *auth_token = 0;
        char *course_id = 0;
        char *filename_arg = 0;
        char *filename = 0;
        char *foldername = 0;
        char *path_arg = 0;
        char *p = 0;
        size_t len = 0;
        char *built_uri = 0;
        int opt = 0;

        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "c:n:p:")) != -1) {
                switch (opt) {
                case 'c':
                        course_id = optarg;
                        break;
                case 'n':
                        filename_arg = optarg;
                        break;
                case 'p':
                        path_arg = optarg;
                        break;
                default:
                        break;
                }
        }

        if (!course_id) {
                ret = EINVAL;
                fprintf(stderr, "course-id is mandatory\n");
                goto cleanup;
        }

        if (!filename_arg) {
                ret = EINVAL;
                fprintf(stderr, "filename is mandatory\n");
                goto cleanup;
        } else {
                while (filename_arg &&
                       filename_arg[0] == '/') {
                        filename_arg++;
                }

                len = strlen(filename_arg);

                if (!(filename = calloc(len + 1, sizeof *filename))) {
                        ret = errno;
                        perror(L("calloc"));
                        goto cleanup;
                }

                sprintf(filename, "%s", filename_arg);
                p = strrchr(filename, '/');

                if (p) {
                        foldername = filename;
                        *p = 0;
                        filename = p + 1;
                }
        }

        if (!filename ||
            !filename[0]) {
                ret = EINVAL;
                fprintf(stderr, "invalid filename\n");
                goto cleanup;
        }

        if (!path_arg) {
                ret = EINVAL;
                fprintf(stderr, "path is mandatory\n");
                goto cleanup;
        }

        curl_global_init(CURL_GLOBAL_DEFAULT);

        if (!(url_base = get_url_base())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        if (!(auth_token = get_auth_token())) {
                ret = ENOENT;

                /* Error should have already been printed */
                goto cleanup;
        }

        /* Step 1 */
        len = snprintf(0, 0, "%s/api/v1/courses/%s/files", url_base, course_id);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto cleanup;
        }

        if (!(built_uri = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto cleanup;
        }

        sprintf(built_uri, "%s/api/v1/courses/%s/files", url_base, course_id);

        if ((ret = file_upload(built_uri, auth_token, filename, path_arg,
                               foldername))) {
                goto cleanup;
        }

        ret = 0;
cleanup:
        free(url_base);
        free(auth_token);
        curl_global_cleanup();

        return ret;
}
